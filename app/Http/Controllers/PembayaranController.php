<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

use App\Models\Bku;
use App\Models\Akad;
use App\Models\Biaya_titip;
use App\Models\Administrasi;

use Auth;
use Carbon\Carbon;

class PembayaranController extends Controller
{
	public function __construct(
    							Bku $bku,
    							Akad $akad,
                                Request $request,
    							Biaya_titip $biaya_titip,
                                Administrasi $administrasi
                            )
    {
        $this->bku  	    = $bku;
    	$this->akad  	    = $akad;
        $this->request      = $request;
        $this->biaya_titip  = $biaya_titip;
        $this->administrasi = $administrasi;

        view()->share([
            'menu'          => 'pembayaran',
            'subMenu'       => '',
            'menuHeader'    => config('library.menu_header'),
        ]);
    }

    public function pendapatan()
    {
        $biayaTitip         = $this->biaya_titip();
        $administrasi       = $this->administrasi();

        // return $biayaTitip->data;

        // list column 'list biaya titip' and 'list biaya administrasi'
        $columnBiayaTitip           = config('library.column.pendapatan.list_biaya_titip');
        $columnBiayaAdministrasi    = config('library.column.pendapatan.list_biaya_administrasi');

    	return $this->template('pembayaran.pendapatan', compact(
            'columnBiayaTitip', 'columnBiayaAdministrasi', 
            'administrasi', 'biayaTitip'
        ));
    }

    // for table 'LIST BIAYA TITIP'
    public function biaya_titip()
    {
        // $akad = $this->akad->baseBranch()->joinNasabah()->joinBiayaTitip();
        $akad = $this->biaya_titip;

        // for base branch show from table 'biaya titip'
        // $akad = $akad->where('no_id', 'LIKE', '%c99-'.$this->infoCabang()->nomorCabang.'%');
        $akad = $akad->where('no_id', 'C99-04-290619-003');
        $akad = $akad->sorted('tanggal_pembayaran', 'desc');
        $akad = $akad->sorted('no_id', 'desc');
        $akad = $this->filter($akad)->akad;
        
        $dateRange  = $this->filter($akad)->dateRange;

        $data       = $akad->get();
        $total      = $this->total_pembayaran();

        return (object) compact('total', 'data', 'dateRange');
    }

    public function total_pembayaran()
    {
        $total  = $this->biaya_titip;
        $total  = $total->sorted('tanggal_pembayaran', 'desc');
        $total  = $total->sorted('no_id', 'desc');
        $total  = $total->first()->saldo;

        return nominal($total);
    }

    public function filter($akad)
    {
        if(request('daterange')){
            $endDate    = carbon::parse(substr(request('daterange'), 13, 20));
            $startDate  = carbon::parse(substr(request('daterange'), 1, 9));
        }else{
            $endDate    = Carbon::now();
            $startDate  = Carbon::now()->subMonths(3)->startOfMonth();
        }

        $akad = $akad->whereBetween('tanggal_pembayaran', [$startDate, $endDate]);

        if(request('by')){
            $akad = $akad->where(request('by'), 'LIKE', '%'.request('q').'%');
        }

        if(request()->has('type_money')){
           if(request('type_money') == 'kredit'){
               $akad = $akad->where('kredit', '!=', 0);
           }elseif(request('type_money') == 'debit'){
               $akad = $akad->where('pembayaran', '!=', 0);
           }
        }

        $dateRange  = $startDate->format('m/d/Y').' - '.$endDate->format('m/d/Y');

        return (object) compact('akad', 'dateRange');
    }

    // for table 'LIST BIAYA ADMINISTRASI'
    public function administrasi()
    {
        $akad = $this->akad->joinNasabah()->baseBranch();
        $akad = $akad->sorted('tanggal_akad', 'desc');
        $akad = $akad->paginate(10);

        return $akad;
    }

    public function cair_pendapatan()
    {
        $total      = remove_dot(request('total'));
        $nominal    = remove_dot(request('nominal'));
        $keterangan = remove_dot(request('keterangan'));

        $hasil_total= $total - $nominal;

        $no_id      = $this->codeNoId();

        $data_bt = $this->biaya_titip;
        $data_bt->no_id                 = $no_id;
        $data_bt->saldo                 = $hasil_total;
        $data_bt->kredit                = $nominal;
        $data_bt->pembayaran            = 0;
        $data_bt->keterangan            = $keterangan.' DI CAIRKAN OLEH '.Auth::user()->username;
        $data_bt->tanggal_pembayaran    = Carbon::now()->format('Y-m-d');
        $data_bt->save();

        return redirect()->back();
    }

    public function codeNoId()
    {
        /*
        * format code 'nomor id' :
        * c99-04-021019-01
        * 'kode citra99 - nomor cabang - tanggal cair uang - code Cair uang - cair uang yang keberapa pada hari itu'
        */
        
        $codeNoId       = 'c99-'.$this->infoCabang()->nomorCabang.'-'.Carbon::now()->format('dmy').'-CU';

        // 'mendapatkan jumlah akad ke-berapa pada hari ini'
        $contractToday  = $this->biaya_titip->where('no_id', 'LIKE', '%'.$codeNoId.'%')->count();
        $contractToday  = $contractToday + 1;
        $contractToday  = $contractToday >= 10 ? '-0'.$contractToday : '-00'.$contractToday;
        
        $value          = $codeNoId . $contractToday;

        return $value;
    }

    public function reset_pendapatan()
    {
        $id = request('id');

        $biaya_titip = $this->biaya_titip->find($id);
        $biaya_titip->delete();

        return 'berhasil';
    }

    public function bku()
    {
        $bku = $this->bku->baseBranch()->jenis('kas')->sorted();

        if(request('by')){
            $bku = $bku->search(request('by'), request('q'));
        }

        $bku = $bku->paginate(request('perpage', 10));

        $column = config('library.column.bku');

    	return $this->template('pembayaran.bku', compact(
            'column', 'bku'
        ));
    }
}

// set default last page
// if(request('page') == '') {
//     $lastPage = $akad->paginate(10)->lastPage();
//     Paginator::currentPageResolver(function() use ($lastPage) {
//         return $lastPage;
//     });
// }
