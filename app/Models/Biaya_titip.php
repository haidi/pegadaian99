<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biaya_titip extends Model
{
    protected $table        = "bea_titip";
    public $timestamps      = false;
    protected $primaryKey   = 'id_bt';
    // protected $guarded   = [];
    protected $fillable     = [
    	'id_bt',
        'no_id',
        'uniq_key',
        'pembayaran',
        'pembayaran_ke',
        'tanggal_pembayaran',
        'keterangan',
    ];

    public function akad()
    {
        return $this->belongsTo('App\Models\Akad', 'no_id', 'no_id');
    }

    public function scopeJoinAkad($query)
    {
        $query->rightJoin('akad', 'bea_titip.no_id', '=', 'akad.no_id');

        $user_cabang    = User_cabang::baseUsername()->first();

        return $query->where('akad.id_cabang', $user_cabang->id_cabang);
    }

    public function scopeJoinNasabah($query, $status = 'bersangkutan')
    {
        $query->leftJoin('nasabah', 'akad.key_nasabah', '=', 'nasabah.key_nasabah')
                     ->where('nasabah.status_nasabah', $status);
    }

    public function scopeSorted($query, $by = 'id_bt', $sort = 'asc')
    {
        return $query->orderBy($by, $sort);
    }

    public function getNamaLengkapAttribute()
    {
        if($this->akad){
            return $this->akad->nasabah->nama_lengkap;
        }else{
            return $this->keterangan;
        }
    }

    public function getNominalPembayaranAttribute()
    {
        return 'Rp. '.nominal($this->pembayaran);
    }

    public function getNominalKreditAttribute()
    {
        return 'Rp. '.nominal($this->kredit);
    }

    public function getNominalSaldoAttribute()
    {
        return 'Rp. '.nominal($this->saldo);
    }

    public function getIdAkadAttribute()
    {
        if($this->akad){
            return $this->akad->id_akad;
        }
    }

    public function getConditionTypeMoneyAttribute()
    {
        if($this->pembayaran != 0){
            return 'debit';
        }elseif($this->kredit != 0){
            return 'kredit';
        }
    }
}
