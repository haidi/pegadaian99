<div class="modal fade" id="modal-edit"  tabindex="-1" aria-hidden="true" style='z-index:10000;' role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title prosedur-title">Pembayaran Biaya Titip</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 pl-4">
                        <a href="#" class="btn btn-sm btn-success" onClick="custom_checkbox('add')">
                            <i class="zmdi zmdi-plus"></i> Tambah
                        </a>
                        <a href="#" class="btn btn-sm btn-warning" onClick="custom_checkbox('delete')">
                            <i class="ion-minus-round"></i> Kurangi
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 pl-4">
                        <h4 id="keterangan_waktu_ke"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 pl-5">
                        <div class="form-group row">
                            <div class="col-sm-12 col-md-12 checkbox" >
                                
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6 pl-4">
                        <h5 id="keterangan_total_bt">Total B.Titip : Rp. 0 (0 minggu) </h5> <br>
                        <h5 id="keterangan_total" style="display:none">Total Pembayaran : Rp. 0 </h5>
                        <input type="hidden" name="nominal_total_final" id="nominal_total_final">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6 pl-4">
                        <a href="#" class="btn btn-sm btn-info bayar disabled" onClick="bayar_prosedur()">
                            Bayar
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-success waves-effect " data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>

    <input type="hidden" class="from_checkbox">
    <input type="hidden" class="until_checkbox">
    <input type="hidden" class="default_until_checkbox">
</div>