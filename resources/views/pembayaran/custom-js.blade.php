<script>
    $(document).ready(function() {
       $('#list_biaya_titip').DataTable();
    //    }).page( 'last' ).draw( 'page' );
   });

   $(function(){
        $('#nominal-bt').on('keyup' ,function(){
            var saldo_bt = '{{$biayaTitip->total}}';
            saldo_bt = remove_dot(saldo_bt);
            saldo_bt = Number(saldo_bt);

            this.value = formatRupiah(this.value);
            value = remove_dot(this.value);
            value = Number(value);

            // console.log(value, saldo_bt);
            if(value > saldo_bt){
                $('.proses-bt').attr('disabled', true);

                // internal function
                notif('Saldo Kurang dari Nominal');
            }else if(saldo_bt == 0 && value > 0){
                $('.proses-bt').attr('disabled', true);

                notif('Maaf, Saldo Anda 0');
            }else if(value <= saldo_bt){
                $('.proses-bt').prop('disabled', false);
            }
        });

        function notif(message)
        {
            swal({
                title: "Pemberitahuan!",
                text: message,
                icon: "warning",
                button: "Oke!",
                dangerMode: true,
            });
        }
   });

    function reset_bt(id)
    {
        swal({
            title: "Pemberitahuan!",
            text: "Anda Yakin Reset Data Ini ??",
            icon: "warning",
            button: "Proses",
        }).then(function(){
            $.ajax({
                url: '{{route("pembayaran.reset-pendapatan")}}',
                type: 'GET',
                cache: false,
                data: {id: id},
                success:function(result){		
                    // console.log('segi server id ='+result);
                    location.reload();
                },
                error:function(xhr, ajaxOptions, thrownError){
                    console.log(thrownError)
                }
            });
        });
    }

    function edit(id)
    {
        $.ajax({
            url: '{{url("akad/ajax/fetch-data")}}',
            type: 'GET',
            cache: false,
            data:{id:id},
            success:function(result){
               modal_edit(result);
            },
            error:function(xhr, ajaxOptions, thrownError){
                console.log(thrownError)
            }
        });
    }

    function modal_edit(data)
    {
        console.log(data);

        if(data.status_tunggakan == 1){
            from = 0;
            until = 0;
        }else if (data.status_tunggakan == 0){
            from = data.waktu_sudah + 1;
            until = data.waktu_sudah + Number(data.waktu_tertunggak);
        }
        
        $('.from_checkbox').val(from);
        $('.until_checkbox').val(until);
        $('.default_until_checkbox').val(until);

        execution_checkbox(from, until);

        $('#modal-edit').modal('show');
    }

    function execution_checkbox(from, until)
    {
        var i           = from;
        // var checked     = type == 'pelunasan' ? 'checked' : '';
        var disabled    = '';
        var checkbox    = '';

        var checked = 'checked';

        // 'agar jika sudah lunas biaya titip, maka tidak muncul checkbox'
        if(from > 0){
            for (i; i <= until; i++) {
                disabled = '';

                checkbox = checkbox + '<div class="checkbox-color checkbox-success checkbox-'+i+'">';
                checkbox = checkbox + '<input id="checkbox'+i+'" type="checkbox" class="checkbox'+i+'" '+checked+' '+disabled+' value="'+i+'" onCLick="condition_disabled('+i+')">';
                checkbox = checkbox + '<label for="checkbox'+i+'" class="checkbox'+i+'">';
                checkbox = checkbox + i;
                checkbox = checkbox + '</label>';
                checkbox = checkbox + '</div>'; 
            }

            $('.checkbox').html(checkbox);
        }else{
            $('.checkbox').empty();
        }
    }

    function custom_checkbox(condition)
    {
        var from            = $('.from_checkbox').val()
        var until           = $('.until_checkbox').val()
        var default_until   = parseInt($('.default_until_checkbox').val())

        if(condition == 'add'){
            until++;
            $('.until_checkbox').val(until)

             // show checkbox base on add time and remove time
            add_remove_checkbox(from, until, condition)
        }

        if(condition == 'delete'){
            if(until > default_until){
                until--;
                $('.until_checkbox').val(until)

                // show checkbox base on add time and remove time
                add_remove_checkbox(from, until, condition)
            }            
        }
    }

    function add_remove_checkbox(from, until, condition)
    {
        checkbox = '';

        // 'untuk mendapatkan nilai checkbox yang tercentang'
        var checked = '';
        $('input[type=checkbox]').each(function () {
            if (this.checked){
                checked = $(this).val();
            }           
        });
        checked = Number(checked) + 2;

        if(until >= checked){
            disabled = 'disabled';
        }else{
            disabled = '';
        }

        checkbox = checkbox + '<div class="checkbox-color checkbox-success checkbox'+until+'">';
        checkbox = checkbox + '<input id="checkbox'+until+'" type="checkbox" class="checkbox'+until+'" '+disabled+' value="'+until+'" onCLick="condition_disabled('+until+')">';
        checkbox = checkbox + '<label for="checkbox'+until+'" class="checkbox'+until+'">';
        checkbox = checkbox + until;
        checkbox = checkbox + '</label>';
        checkbox = checkbox + '</div>'; 

        if(condition == 'add'){
            $('.checkbox').append(checkbox)
        }else if(condition == 'delete'){
            until = until + 1;

            $('.checkbox'+until).remove()
        }
    }
</script>